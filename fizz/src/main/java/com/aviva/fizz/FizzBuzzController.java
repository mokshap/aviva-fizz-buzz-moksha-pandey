package com.aviva.fizz;

import org.springframework.stereotype.Controller;

import com.aviva.fizz.*;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FizzBuzzController {
	private static final String template = "Hello, %s!";
	private final AtomicLong counter = new AtomicLong();

	@RequestMapping("/greeting")
	public FizzBuzzModel greeting(@RequestParam(value = "value", defaultValue = "World") String value) {
		return new FizzBuzzModel(1);
	}
}
