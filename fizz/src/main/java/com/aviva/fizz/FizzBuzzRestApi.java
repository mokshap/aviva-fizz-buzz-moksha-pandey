package com.aviva.fizz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.aviva.fizz.Step1;

@SpringBootApplication(scanBasePackages={"com.aviva.fizz"})
public class FizzBuzzRestApi {
	public static void main(String[] args) {
		SpringApplication.run(FizzBuzzRestApi.class, args);
	}
}
