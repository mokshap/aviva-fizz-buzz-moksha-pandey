package com.aviva.fizz;

import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class Step4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub
		// validate if the entered value is integer between 1 and 1000 than call
		// printValue()method to check if current day is wednesday and print the
		// required output
		int n;
		Scanner s = new Scanner(System.in);
		System.out.print("Enter the number you want to check:");
		n = s.nextInt();
		if (n > 0 && n < 1000) {
			System.out.println("The given number " + n + " is Positive");

			printValue(n);
		} else if (n < 0) {
			System.out.println("The given number " + n + " is Negative");
		} else {
			System.out.println("The given number " + n + " is neither Positive nor Negative or greater thn 1000");
		}

	}

	public static void printValue(int n) {
		Calendar c = Calendar.getInstance();

		// c.setTime(yourDate);
		int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
		if (n % 3 == 0) {

			if (dayOfWeek == 4) {
				System.out.println("wizz");
			} else

			{
				System.out.println("fizz");

			}
		} else if (n % 5 == 0) {

			if (dayOfWeek == 4) {
				System.out.println("wuzz");
			} else {
				System.out.println("buzz");
			}
		} else if (n % 5 == 0 && n % 3 == 0) {
			System.out.println("fizz buzz");
		} else {
			for (int i = 1; i <= n; i++) {
				System.out.println(i + "\n");
			}
		}

	}

}
