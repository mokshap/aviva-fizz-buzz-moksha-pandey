package com.aviva.fizz;

import java.util.Scanner;

/**
 * check if the number entered is a positive integer than call
 * printValue()method to print the required output
 *
 */

public class Step1 {
	public static void main(String[] args) {
		int n;
		Scanner s = new Scanner(System.in);
		System.out.print("Enter the number you want to check:");
		n = s.nextInt();
		if (n > 0) {
			System.out.println("The given number " + n + " is Positive");

			printValue(n);
		} else if (n < 0) {
			System.out.println("The given number " + n + " is Negative");
		} else {
			System.out.println("The given number " + n + " is neither Positive nor Negative ");
		}
	}

	public static void printValue(int n) {
		if (n % 3 == 0) {
			System.out.println("fizz");
		} else if (n % 5 == 0) {
			System.out.println("buzz");
		} else if (n % 5 == 0 && n % 3 == 0) {
			System.out.println("fizz buzz");
		} else {
			for (int i = 1; i <= n; i++) {
				System.out.println(i + "\n");
			}
		}

	}
}
