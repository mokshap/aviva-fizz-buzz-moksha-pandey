package com.aviva.fizz;

import java.io.Serializable;

public class FizzBuzzModel implements Serializable {

	/**
		 * 
		 */
	private static final long serialVersionUID = 1L;
	public int value;

	public FizzBuzzModel(int value) {
		this.value = value;

	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

}
