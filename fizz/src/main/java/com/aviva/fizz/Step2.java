package com.aviva.fizz;

import java.util.Scanner;

public class Step2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// validate if nthe entered value is integer between 1 and 1000
		int n;
		Scanner s = new Scanner(System.in);
		System.out.print("Enter the number you want to check:");
		n = s.nextInt();
		if (n > 0 && n < 1000) {
			System.out.println("The given number " + n + " is Positive");

		} else if (n < 0) {
			System.out.println("The given number " + n + " is Negative");
		} else {
			System.out.println("The given number " + n + " is neither Positive nor Negative or greater thn 1000");
		}

	}

}
